from conans import ConanFile, CMake, tools


class LuajitConan(ConanFile):
    name = "luajit"
    version = "2.0.5"
    license = "MIT"
    author = "toge.mail@gmail.com"
    url = "https://bitbucket.org/toge/conan-luajit/"
    description = "LuaJIT is JIT compiler for the Lua language. "
    topics = ("jit", "luadist", "lua")
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"

    def source(self):
        self.run("git clone https://github.com/LuaDist/luajit")
        self.run("cd luajit && git fetch origin pull/1/head:luajit_temp && git checkout luajit_temp")
        # This small hack might be useful to guarantee proper /MT /MD linkage
        # in MSVC if the packaged project doesn't have variables to set it
        # properly
        tools.replace_in_file("luajit/CMakeLists.txt", "add_library ( liblua SHARED ${LJCORE_C} ${DEPS} )", "add_library ( liblua STATIC ${LJCORE_C} ${DEPS} )")

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="luajit", defs={"LUAJIT_ENABLE_LUA52COMPAT": True, "LUAJIT_DISABLE_FFI": True})
        cmake.build(target="liblua")

        # Explicit way:
        # self.run('cmake %s/hello %s'
        #          % (self.source_folder, cmake.command_line))
        # self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        self.copy("*.h", dst="include", src="luajit/src")
        self.copy("*.hpp", dst="include", src="luajit/src")
        self.copy("luaconf.h", dst="include")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["lua"]
        self.cpp_info.exelinkflags = ["-pagezero_size 10000", "-image_base 100000000"]

